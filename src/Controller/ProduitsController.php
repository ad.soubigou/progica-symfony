<?php

namespace App\Controller;

use App\Entity\Gite;
use App\Repository\GiteRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Request;

#[Route('/produits', name: 'produits_')]
class ProduitsController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(GiteRepository $giteRep, Request $request): Response
    {
        // On récupère les données du formulaire via l'objet Request
        $ville = $request->query->get('ville');
        $gites = $giteRep->filters($ville);
        return $this->render(
            'produits/index.html.twig',
            [
                'gite' => $gites,

            ]
        );
    }

    #[Route('/{id}', name: 'details')]
    public function details(Gite $gite): Response
    {
        $saison = $gite->getSaisons();
        $photos = $gite->getPhotos();
        $jour = new DateTime();
        $date = $jour->format('Y-m-d');
        return $this->render('/produits/details.html.twig', [
            'gite' => $gite,
            'saisons' => $saison,
            'photos' => $photos,
            'jour' => $date
        ]);
    }
}
