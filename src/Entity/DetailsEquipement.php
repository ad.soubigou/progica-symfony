<?php

namespace App\Entity;

use App\Repository\DetailsEquipementRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DetailsEquipementRepository::class)]
class DetailsEquipement
{
    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'detailsEquipements')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Gite $gite = null;

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'detailsEquipements')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Equipement $equipement = null;

    public function getGite(): ?Gite
    {
        return $this->gite;
    }

    public function setGite(?Gite $gite): static
    {
        $this->gite = $gite;

        return $this;
    }

    public function getEquipement(): ?Equipement
    {
        return $this->equipement;
    }

    public function setEquipement(?Equipement $equipement): static
    {
        $this->equipement = $equipement;

        return $this;
    }
}
