<?php

namespace App\Entity;

use App\Repository\EquipementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EquipementRepository::class)]
class Equipement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $label = null;

    #[ORM\Column(length: 100)]
    private ?string $categorie = null;

    /**
     * @var Collection<int, DetailsEquipement>
     */
    #[ORM\OneToMany(targetEntity: DetailsEquipement::class, mappedBy: 'equipement')]
    private Collection $detailsEquipements;

    public function __construct()
    {
        $this->detailsEquipements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;

        return $this;
    }

    public function getCategorie(): ?string
    {
        return $this->categorie;
    }

    public function setCategorie(string $categorie): static
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * @return Collection<int, DetailsEquipement>
     */
    public function getDetailsEquipements(): Collection
    {
        return $this->detailsEquipements;
    }

    public function addDetailsEquipement(DetailsEquipement $detailsEquipement): static
    {
        if (!$this->detailsEquipements->contains($detailsEquipement)) {
            $this->detailsEquipements->add($detailsEquipement);
            $detailsEquipement->setEquipement($this);
        }

        return $this;
    }

    public function removeDetailsEquipement(DetailsEquipement $detailsEquipement): static
    {
        if ($this->detailsEquipements->removeElement($detailsEquipement)) {
            // set the owning side to null (unless already changed)
            if ($detailsEquipement->getEquipement() === $this) {
                $detailsEquipement->setEquipement(null);
            }
        }

        return $this;
    }
}
