<?php

namespace App\Entity;

use App\Repository\DetailsServiceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DetailsServiceRepository::class)]
class DetailsService
{
    #[ORM\Column]
    private ?float $tarif = null;

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'detailsServices')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Service $service = null;

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'detailsServices')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Gite $gite = null;

    public function getTarif(): ?float
    {
        return $this->tarif;
    }

    public function setTarif(float $tarif): static
    {
        $this->tarif = $tarif;

        return $this;
    }

    public function getService(): ?Service
    {
        return $this->service;
    }

    public function setService(?Service $service): static
    {
        $this->service = $service;

        return $this;
    }

    public function getGite(): ?Gite
    {
        return $this->gite;
    }

    public function setGite(?Gite $gite): static
    {
        $this->gite = $gite;

        return $this;
    }
}
