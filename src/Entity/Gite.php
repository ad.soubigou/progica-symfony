<?php

namespace App\Entity;

use App\Repository\GiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GiteRepository::class)]
class Gite
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    private ?string $adresse = null;

    #[ORM\Column(length: 5)]
    private ?string $code_postal = null;

    #[ORM\Column]
    private ?float $surface = null;

    #[ORM\Column]
    private ?int $nb_chambre = null;

    #[ORM\Column]
    private ?int $nb_couchage = null;

    #[ORM\Column]
    private ?int $capacite = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column]
    private ?int $nb_animaux = null;

    #[ORM\ManyToOne(inversedBy: 'gites')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Proprietaire $proprietaire = null;

    #[ORM\ManyToOne(inversedBy: 'gites')]
    private ?Contact $contact = null;

    #[ORM\ManyToOne(inversedBy: 'gites')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Ville $ville = null;

    #[ORM\ManyToOne(inversedBy: 'gites')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Region $region = null;

    #[ORM\ManyToOne(inversedBy: 'gites')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Departement $departement = null;

    /**
     * @var Collection<int, Saison>
     */
    #[ORM\OneToMany(targetEntity: Saison::class, mappedBy: 'gite')]
    private Collection $saisons;

    /**
     * @var Collection<int, DetailsEquipement>
     */
    #[ORM\OneToMany(targetEntity: DetailsEquipement::class, mappedBy: 'gite')]
    private Collection $detailsEquipements;

    /**
     * @var Collection<int, DetailsService>
     */
    #[ORM\OneToMany(targetEntity: DetailsService::class, mappedBy: 'gite')]
    private Collection $detailsServices;

    /**
     * @var Collection<int, Photos>
     */
    #[ORM\OneToMany(targetEntity: Photos::class, mappedBy: 'gite', orphanRemoval: true)]
    private Collection $photos;

    public function __construct()
    {
        $this->saisons = new ArrayCollection();
        $this->detailsEquipements = new ArrayCollection();
        $this->detailsServices = new ArrayCollection();
        $this->photos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): static
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->code_postal;
    }

    public function setCodePostal(string $code_postal): static
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    public function getSurface(): ?float
    {
        return $this->surface;
    }

    public function setSurface(float $surface): static
    {
        $this->surface = $surface;

        return $this;
    }

    public function getNbChambre(): ?int
    {
        return $this->nb_chambre;
    }

    public function setNbChambre(int $nb_chambre): static
    {
        $this->nb_chambre = $nb_chambre;

        return $this;
    }

    public function getNbCouchage(): ?int
    {
        return $this->nb_couchage;
    }

    public function setNbCouchage(int $nb_couchage): static
    {
        $this->nb_couchage = $nb_couchage;

        return $this;
    }

    public function getCapacite(): ?int
    {
        return $this->capacite;
    }

    public function setCapacite(int $capacite): static
    {
        $this->capacite = $capacite;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getNbAnimaux(): ?int
    {
        return $this->nb_animaux;
    }

    public function setNbAnimaux(int $nb_animaux): static
    {
        $this->nb_animaux = $nb_animaux;

        return $this;
    }

    public function getProprietaire(): ?Proprietaire
    {
        return $this->proprietaire;
    }

    public function setProprietaire(?Proprietaire $proprietaire): static
    {
        $this->proprietaire = $proprietaire;

        return $this;
    }

    public function getContact(): ?Contact
    {
        return $this->contact;
    }

    public function setContact(?Contact $contact): static
    {
        $this->contact = $contact;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): static
    {
        $this->ville = $ville;

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): static
    {
        $this->region = $region;

        return $this;
    }

    public function getDepartement(): ?Departement
    {
        return $this->departement;
    }

    public function setDepartement(?Departement $departement): static
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * @return Collection<int, Saison>
     */
    public function getSaisons(): Collection
    {
        return $this->saisons;
    }

    public function addSaison(Saison $saison): static
    {
        if (!$this->saisons->contains($saison)) {
            $this->saisons->add($saison);
            $saison->setGite($this);
        }

        return $this;
    }

    public function removeSaison(Saison $saison): static
    {
        if ($this->saisons->removeElement($saison)) {
            // set the owning side to null (unless already changed)
            if ($saison->getGite() === $this) {
                $saison->setGite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, DetailsEquipement>
     */
    public function getDetailsEquipements(): Collection
    {
        return $this->detailsEquipements;
    }

    public function addDetailsEquipement(DetailsEquipement $detailsEquipement): static
    {
        if (!$this->detailsEquipements->contains($detailsEquipement)) {
            $this->detailsEquipements->add($detailsEquipement);
            $detailsEquipement->setGite($this);
        }

        return $this;
    }

    public function removeDetailsEquipement(DetailsEquipement $detailsEquipement): static
    {
        if ($this->detailsEquipements->removeElement($detailsEquipement)) {
            // set the owning side to null (unless already changed)
            if ($detailsEquipement->getGite() === $this) {
                $detailsEquipement->setGite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, DetailsService>
     */
    public function getDetailsServices(): Collection
    {
        return $this->detailsServices;
    }

    public function addDetailsService(DetailsService $detailsService): static
    {
        if (!$this->detailsServices->contains($detailsService)) {
            $this->detailsServices->add($detailsService);
            $detailsService->setGite($this);
        }

        return $this;
    }

    public function removeDetailsService(DetailsService $detailsService): static
    {
        if ($this->detailsServices->removeElement($detailsService)) {
            // set the owning side to null (unless already changed)
            if ($detailsService->getGite() === $this) {
                $detailsService->setGite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Photos>
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photos $photo): static
    {
        if (!$this->photos->contains($photo)) {
            $this->photos->add($photo);
            $photo->setGite($this);
        }

        return $this;
    }

    public function removePhoto(Photos $photo): static
    {
        if ($this->photos->removeElement($photo)) {
            // set the owning side to null (unless already changed)
            if ($photo->getGite() === $this) {
                $photo->setGite(null);
            }
        }

        return $this;
    }
}
