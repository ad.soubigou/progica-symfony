<?php

namespace App\Repository;

use App\Entity\Gite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Gite>
 */
class GiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gite::class);
    }

    public function filters(?string $ville)
    {
        // On crée un queryBuilder pour l'entité Gite
        $qb = $this->createQueryBuilder('gite')
        // On joint la table/entité Ville via la propriété ville de l'entité Gite
                   ->join('gite.ville', 'ville')
        // On ajoute les champs de la table Ville dans la requête
                   ->addSelect('ville');

        if ($ville) {
            // On ajoute une condition pour sélectionner la ville recherchée
            $qb->andWhere('ville.nom = :ville')
               ->setParameter('ville', $ville);
        }

        return $qb->getQuery()->getResult();
    }
}